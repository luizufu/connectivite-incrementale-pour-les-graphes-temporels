\documentclass{beamer}

\usepackage[citestyle=reading]{biblatex}
\addbibresource{references.bib}

\usepackage[utf8]{inputenc}

\usepackage{microtype, booktabs}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{changepage}

\usepackage[noend]{algpseudocode}
\usepackage{algorithm}
\MakeRobust{\Call}

\usepackage{tikz}
\usetikzlibrary{arrows,automata,shapes,calc,positioning}

\newcommand{\mund}{\mathunderscore}

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}


\title{Dynamic data structure for temporal connectivity: the unsorted incremental case}
\author{
        Luiz Fernando Afra Brito \\
        Marcelo Keese Albertini \\
        Bruno Travençolo \\
        Federal University of Uberlândia, Brazil\\
            \and\\
        Arnaud Casteigts\\
        University of Bordeaux, France
}

\AtBeginSection[]
{
  \begin{frame}{Table of Contents}
    \tableofcontents[currentsection,%
        sectionstyle=show/shaded,%
        subsectionstyle=show/show/shaded,%
    ]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}{Table of Contents}
    \tableofcontents[currentsection,%
        sectionstyle=show/shaded,%
        subsectionstyle=show/shaded/shaded,%
    ]
  \end{frame}
}

\begin{document}

\begin{frame}
 \titlepage
\end{frame}

\begin{frame}{Table of Contents}
\tableofcontents
\end{frame}



\section{Introduction}

\begin{frame}{Motivation}
Assuming that input is incremental and not temporally ordered
\vspace{.5em}
\begin{itemize}
    \setlength\itemsep{1.5em}
    \item maintain temporal connectivity information
    \item provide queries to check temporal connectivity between vertices
    \item generate traces that make vertices temporally connected
\end{itemize}

\vspace{3em}
\uncover<2>{
    Application example:
    \vspace{.5em}
    \begin{itemize}
        \setlength\itemsep{.5em}
        \item COVID-19 cases happens continually
        \item maintain contamination graph
        \item test/visualize/measure current chains of contamination
    \end{itemize}

}
% In epidemics, these queries are essential in order to describe the current situation of the disease.
% During an epidemic scenario, data containing the interaction details among entities are collected incrementally and non-chronologically ordered registry are added to the database.
% Then, these data are accessed in order to detect whether an entity was infected or not by some suspected source.
% Furthermore, data can also be used to reconstruct the behavior of the spreading disease given a source or target entity.
% In this context, it is interesting to know the many ways a disease can spread.
% For example, reconstructing the earliest, latest and fastest way of occurring a infection can provide useful information.

\end{frame}

\begin{frame}{Operations}
\begin{description}[${can\mund{}reach(u, v, [t_1, t_\kappa])}$]
    \setlength\itemsep{1.5em}
    \item[${add\mund{}contact(u, v, [t_1, t_\kappa])}$] adds $((u, v), t)$ for $\forall t \in [t_1, t_\kappa]$
    \item[${can\mund{}reach(u, v, [t_1, t_\kappa])}$] is there $\mathcal{J}$ from $u$ to $v$ in $[t_1, t_\kappa]$
    \item[${get\mund{}journey(u, v, [t_1, t_\kappa])}$] materializes $\mathcal{J}$ from $u$ to $v$ in $[t_1, t_\kappa]$
\end{description}

\vspace{3em}
\uncover<2>{
    There can be many $\mathcal{J}$ from $u$ to $v$, which is the better?
    \vspace{.5em}
    \begin{itemize}
        \item shortest? fastest? foremost? latest?
    \end{itemize}
}
\end{frame}

\begin{frame}{Simple solution}
\begin{itemize}
    \setlength\itemsep{1.5em}
    \item Maintain contact list $C$ sorted by timestamp
    \item In query time, perform DFS taking into account timestamps
    \item To reconstruct journey, do backtrack and concatenate pieces
\end{itemize}

\vspace{3em}
\uncover<2>{
    Costs?
    \vspace{.5em}
    \begin{itemize}
        \setlength\itemsep{.5em}
        \item $O(|C|)$ space
        \item $add\mund{}contact(u, v, [t_1, t_\kappa])$ is $O(\log|C|)$
        \item $can\mund{}reach(u, v, [t_1, t_\kappa])$ is $O(|C|)$
        \item $get\mund{}journey(u, v, [t_1, t_\kappa])$ is $O(|C| + |\mathcal{J}|)$
    \end{itemize}
}
\end{frame}

\begin{frame}{Goals}
\begin{itemize}
    \setlength\itemsep{1.5em}
    \item Precompute everything during ${add\mund{}contact(u, v, [t_1, t_\kappa])}$
    \begin{itemize}
        \item $O(n^2 \tau)$ space?
        \item $O(n^2 (\log\tau + \kappa))$ time? $O(n^2 (\kappa\log\tau))$? $O(n^2 (\tau + \kappa))$?
    \end{itemize}
    \item Perform $can\mund{}reach(u, v, [t_1, t_\kappa])$ as fast as possible
    \begin{itemize}
        \item $O(1)$ time? $O(\log\tau)$?
    \end{itemize}
    \item Perform $get\mund{}journey(u, v, [t_1, t_\kappa])$ as fast as possible
    \begin{itemize}
        \item $O(|\mathcal{J}|)$ time? $O(\log\tau + |\mathcal{J}|)$? $O(\|\mathcal{J}|\log\tau )$?
    \end{itemize}

\end{itemize}
\end{frame}

\begin{frame}{Goals}
\begin{itemize}
    \setlength\itemsep{1.5em}
    \item Precompute everything during ${add\mund{}contact(u, v, [t_1, t_\kappa])}$
    \begin{itemize}
        \item \alert{$O(n^2 \tau)$} space?
        \item \alert{$O(n^2 (\log\tau + \kappa))$} time? $O(n^2 (\kappa\log\tau))$? $O(n^2 (\tau + \kappa))$?
    \end{itemize}
    \item Perform $can\mund{}reach(u, v, [t_1, t_\kappa])$ as fast as possible
    \begin{itemize}
        \item $O(1)$ time? \alert{$O(\log\tau)$}?
    \end{itemize}
    \item Perform $get\mund{}journey(u, v, [t_1, t_\kappa])$ as fast as possible
    \begin{itemize}
        \item $O(|\mathcal{J}|)$ time? \alert{$O(\log\tau + |\mathcal{J}|)$}? $O(|\mathcal{J}|\log\tau)$?
    \end{itemize}

\end{itemize}
\end{frame}


\section{Previous Work}

\begin{frame}{Previous Work}
\centering
\begin{tabular}{l|c|c} \toprule
    & Chronological & Non-chronological \\ \midrule
    Only-incremental & Arnaud's work\footcite{barjon2014testing} & \alert{Chinese's work\footcite{wu2016reachability}} \\\midrule
Only-decremental &  ? & ? \\\midrule
Fully dynamic  & ? & ? \\ \bottomrule
\end{tabular}
\end{frame}

\begin{frame}{Only-Incremental Non-Chronologically Sorted Solution}
    {Usage of incomplete reachability oracle}

\begin{itemize}
    \setlength\itemsep{1.5em}
    \item Transform $\mathcal{G}$ into DAG $G$
    \begin{itemize}
        \item e.g. $(u, v, t) \rightarrow (u \cdot t \cdot out, v \cdot (t + 1) \cdot in)$
    \end{itemize}
    \item Solve the problem of connectivity in DAG
    \begin{itemize}
        \item construct reachability oracle by maintaining $L_{out}(v)$ and $L_{in}(v)$
        \item oracle answers \texttt{YES} whether $L_{out}(u) \cap L_{in}(v) \neq \emptyset$
        \item the perfect oracle would use the smallest $L_{out}(v)$ and $L_{in}(v)$
    \end{itemize}
\end{itemize}


\vspace{2em}
\uncover<2>{
    Particuliarities:
    \vspace{.5em}
    \begin{itemize}
        \setlength\itemsep{.5em}
        \item need some bookkeeping and extra work for temporal queries
        \item falls back to BFS strategy if oracle answers \texttt{NO}
        \item take parameter to limit the size of $L_{out}(v)$ and $L_{in}(v)$
        \begin{itemize} \item oracle will answer more or less \texttt{NO}'s
        \end{itemize}
    \end{itemize}
}

\end{frame}


\section{Definitions}

\begin{frame}{Temporal Graph}
\begin{itemize}
    \setlength\itemsep{1.5em}
    \item $\mathcal{G}  = (V, E, \mathcal{T}, \rho, \zeta)$
        \hspace{3em}(temporal graph)
    \item $\rho : E \times \mathcal{T} \mapsto \{0, 1\}$
        \hspace{2.8em}(presence function)
    \item $\zeta : E \times \mathcal{T} \mapsto \mathbb{T}$
        \hspace{4.6em}(latency function)
\end{itemize}

\vspace{3em}
\uncover<2>{
    \begin{block}{Constant latency temporal graph $\delta\text{-}\mathcal{G}$}
        \vspace{.5em}
        \begin{itemize}
            \setlength\itemsep{.5em}
            \item same as $\zeta : E \times \mathcal{T} \mapsto \delta$
            \item we study only $\delta = 0$ or $\delta = 1$
        \end{itemize}
    \end{block}
}
\end{frame}

\begin{frame}{Journey}
\begin{itemize}
    \setlength\itemsep{1.5em}
    \item $\mathcal{J} = \{c_1, c_2, \ldots, c_k\}$, where $c_i = ((u_i, v_i), t_i)$
    \item $from(\mathcal{J}) = u_1$, \hspace{2.8em} $to(\mathcal{J}) = v_k$
    \item $departure(\mathcal{J}) = t_1$, \hspace{1em} $arrival(\mathcal{J}) = t_k + \delta$
    \uncover<2,3>{\alert{
        \item $v_i = u_{i + 1}$ (thank you Bruno)
    }}
\end{itemize}


\vspace{3em}
\uncover<3>{
    Must respect $t_{i - 1} + \delta \leq t_i$
    \vspace{.5em}
    \begin{itemize}
        \setlength\itemsep{.5em}
        \item if $\delta = 0$, then $\mathcal{J}$ is a non-strict journey
        \item if $\delta = 1$, then $\mathcal{J}$ is a strict journey
    \end{itemize}
}
\end{frame}

\begin{frame}{$R$-Tuples}
\begin{itemize}
    \setlength\itemsep{1.5em}
    \item $r = \{ u, v, t^-, t^+ \}$
    \item $u = from(\mathcal{J})$,  \hspace{3.5em} $v = to(\mathcal{J})$
    \item $t^- = departure(\mathcal{J})$, \hspace{1em} $t^+ = arrival(\mathcal{J}) + \delta$
\end{itemize}

\vspace{3em}
\uncover<2>{
    We work with only the minimal scheduling information
    \vspace{.5em}
    \begin{itemize}
        \setlength\itemsep{.5em}
        \item pos: less space and, therefore, less complexity
        \item neg: can't access journeys directly (need reconstruction)
    \end{itemize}
}
\end{frame}

\begin{frame}{Transitive Closure}
\begin{itemize}
    \setlength\itemsep{1.5em}
    \item $C = (V, E)$
    \item If $can\mund{}reach(u, v, \mathcal{T})$ then $(u, v) \in E$
\end{itemize}

\vspace{3em}
\uncover<2>{
    What can we do:
    \vspace{.5em}
    \begin{itemize}
        \setlength\itemsep{.5em}
        \item can check \alert{only} if there is journey in $\mathcal{T}$
        \item \alert{cannot} reconstruct journeys
        \item \alert{cannot} maintain this incrementally (not straighforward)
    \end{itemize}
}
\end{frame}

\begin{frame}{Timed Transitive Closure}
\begin{itemize}
    \setlength\itemsep{1.5em}
    \item $C = (V, E, \lambda)$
    \item If $can\mund{}reach(u, v, \mathcal{T})$ then $(u, v) \in E$
    \only<1,5,6>{\item $\lambda((u, v)) = \{ (t^-_1, t^+_1), (t^-_2, t^+_2), \ldots, (t^-_l, t^+_l) \}$}
    \only<2>{\item $\lambda((\alert{u}, \alert{v})) = \{ (\alert{t^-_1}, \alert{t^+_1}), (t^-_2, t^+_2), \ldots, (t^-_l, t^+_l) \}$~~~~\alert{$(u, v, t^-_1, t^+_1)$}}
    \only<3>{\item $\lambda((\alert{u}, \alert{v})) = \{ (t^-_1, t^+_1), (\alert{t^-_2}, \alert{t^+_2}), \ldots, (t^-_l, t^+_l) \}$~~~~\alert{$(u, v, t^-_2, t^+_2)$}}
    \only<4>{\item $\lambda((\alert{u}, \alert{v})) = \{ (t^-_1, t^+_1), (t^-_2, t^+_2), \ldots, (\alert{t^-_l}, \alert{t^+_l}) \}$~~~~\alert{$(u, v, t^-_l, t^+_l)$}}
    \uncover<5,6>{\item $R^{u, v} = \{ (u, v, t^-_1, t^+_1), (u, v, t^-_2, t^+_2), \ldots, (u, v, t^-_l, t^+_l)\}$}
\end{itemize}

\vspace{3em}
\uncover<6>{
    What can we do:
    \vspace{.5em}
    \begin{itemize}
        \setlength\itemsep{.5em}
        \item can check \alert{all} journey schedulings in $\mathcal{T}$ or subsets of $\mathcal{T}$
        \item \alert{can} reconstruct journeys by concatenating sub journeys
        \item \alert{can} maintain this incrementally (previous information help)
    \end{itemize}
}

\end{frame}

\begin{frame}{Relations for Intervals}


Given \textcolor{red}{$l_1 = [t^-_1, t^+_1]$} and \textcolor{blue}{$l_2 = [t^-_2, t^+_2]$} in $\lambda((u, v))$

\vspace{1.5em}
\begin{block}{Inclusion relation $(\mathcal{I}, \subseteq)$}
    $\textcolor{red}{l_1} \subseteq \textcolor{blue}{l_2} \longleftrightarrow \textcolor{blue}{t^-_2} \leq \textcolor{red}{t^-_1} \leq \textcolor{red}{t^+_1} \leq \textcolor{blue}{t^+_2}$
\end{block}

\vspace{1.5em}
\begin{block}{Precedence relation $(\mathcal{I}, \prec)$}
    $\textcolor{red}{l_1} \prec \textcolor{blue}{l_2} \longleftrightarrow \textcolor{red}{t^+_1} \leq \textcolor{blue}{t^-_2}$
\end{block}
\end{frame}


\begin{frame}{Extension for $R$-Tuples}
Given \textcolor{red}{$r_1 = (u, v, t^-_1, t^+_1)$} and \textcolor{blue}{$r_2 = (u, v, t^-_2, t^+_2)$} in $R^{u,v}$

\vspace{1em}
\begin{block}{Inclusion relation $(R^{u, v}, \subseteq)$}
    $\textcolor{red}{r_1} \subseteq \textcolor{blue}{r_2} \longleftrightarrow \textcolor{red}{[t^-_1, t^+_1]} \subseteq \textcolor{blue}{[t^-_2, t^+_2]}$
\end{block}

\vspace{3em}

    Given \textcolor{red}{$r_1 = (u, w, t^-_1, t^+_1)$} in $R^{u, w}$ and \textcolor{blue}{$r_2 = (w, v, t^-_2, t^+_2)$} in $R^{w, v}$

\vspace{1em}
\begin{block}{Precedence relation $(R, \prec)$}
    $\textcolor{red}{r_1} \prec \textcolor{blue}{r_2} \longleftrightarrow \textcolor{red}{[t^-_1, t^+_1]} \prec \textcolor{blue}{[t^-_2, t^+_2]}$
\end{block}

\end{frame}


\begin{frame}{Other Operations}
    Given \textcolor{red}{$r_1 = (u, w, t^-_1, t^+_1)$} in $R^{u, w}$ and \textcolor{blue}{$r_2 = (w, v, t^-_2, t^+_2)$} in $R^{w,v}$

\vspace{1.5em}
\begin{block}{Concatenation of $r$-tuples}
    If $\textcolor{red}{r_1} \prec \textcolor{blue}{r_2}$, then $\textcolor{red}{r_1} \cdot \textcolor{blue}{r_2} = (\textcolor{red}{u}, \textcolor{blue}{v}, \textcolor{red}{t^-_1}, \textcolor{blue}{t^+_2})$.
\end{block}
\end{frame}


\section{Number of Tuples}


\begin{frame}{Unnecessary $R$-Tuples}
Given \textcolor{red}{$r_1 = (u, v, t^-_1, t^+_1)$} and \textcolor{blue}{$r_2 = (u, v, t^-_2, t^+_2)$} in $R^{u,v}$

\vspace{1.5em}
\begin{block}{Hypothesis 1}
If $\textcolor{red}{r_1} \subseteq \textcolor{blue}{r_2}$, then, \textcolor{blue}{$r_2$} is not necessary to maintain $R^{u, v}$
\end{block}

\vspace{1.5em}
\begin{block}{Proof (contradiction)}
    \begin{itemize}
        \item Assume that \textcolor{blue}{$r_2$} IS necessary to maintain $R^{u, v}$.
        \item Suppose \textcolor{red}{$r_1$} and \textcolor{blue}{$r_2$} describes, respectively, \textcolor{red}{$\mathcal{J}_1$} and \textcolor{blue}{$\mathcal{J}_2$}.
        \item Since $\textcolor{red}{r_1} \subseteq \textcolor{blue}{r_2}$, we can emulate \textcolor{blue}{$\mathcal{J}_2$} using \textcolor{red}{$\mathcal{J}_1$}
        \begin{itemize}
            \item wait $\textcolor{red}{departure(\mathcal{J}_1)} - \textcolor{blue}{departure(\mathcal{J}_2)}$ instances of time
            \item use \textcolor{red}{$\mathcal{J}_1$}
            \item wait $\textcolor{blue}{arrival(\mathcal{J}_2)} - \textcolor{red}{arrival(\mathcal{J}_1)}$ instances of time
        \end{itemize}
    \item Therefore, \textcolor{blue}{$r_2$} is not necessary but we assumed it was
    \end{itemize}
\end{block}
\end{frame}


\begin{frame}{Number of $R$-Tuples}{Upper Bound}
    Given that $R^{u, v}$ does not contain unnecessary $r$-tuples, i.e. for $\forall(\textcolor{red}{r_1}, \textcolor{blue}{r_2}) \in R^{u, v}$ $\textcolor{red}{r_1} \not\subseteq \textcolor{blue}{r_2}$

\vspace{1.5em}
\begin{block}{Hypothesis 2}
The number of $r$-tuples in $R^{u, v}$ is at most $\tau$.
\end{block}

\vspace{1.5em}
\begin{block}{Proof (contradiction)}
\begin{itemize}
    \item Assume that there are more than $\tau$ $r$-tuples in $R^{u, v}$
    \item Suppose that $r$-tuples have unit intervals shifted by one
    \item Therefore, there can be more than $\tau$ $r$-tuples but the timeline is bounded by $\mathcal{T} = [1, \tau]$
\end{itemize}
\end{block}
\end{frame}

\begin{frame}{Number of $R$-Tuples}{Lower Bound}
    Given that $R^{u, v}$ does not contain unnecessary $r$-tuples, i.e. for $\forall(\textcolor{red}{r_1}, \textcolor{blue}{r_2}) \in R^{u, v}$ $\textcolor{red}{r_1} \not\subseteq \textcolor{blue}{r_2}$

\vspace{1.5em}
\begin{block}{Hypothesis 3}
The number of $r$-tuples in $R^{u, v}$ is at least $\tau$.
\end{block}

\vspace{1.5em}
\begin{block}{Proof (contradiction)}
\begin{itemize}
    \item Assume that there are less than $\tau$ $r$-tuples in $R^{u, v}$
    \item Consider the complete temporal graph $\mathcal{K}_{n, \tau}$
    \begin{itemize}
        \item construct a complete graph $K_n$
        \item activate every edge during all $t \in [1, \tau]$.
    \end{itemize}
    \item $R^{u,v}$ is the set of $r$-tuples of fixed unit intervals shifted by one
    \item Therefore, there can be $\tau$ $r$-tuples but we said it was less
\end{itemize}
\end{block}
\end{frame}

\begin{frame}{Number of $R$-Tuples}{Tight Bounds}
    Given that $R^{u, v}$ does not contain unnecessary $r$-tuples, i.e. for $\forall(\textcolor{red}{r_1}, \textcolor{blue}{r_2}) \in R^{u, v}$ $\textcolor{red}{r_1} \not\subseteq \textcolor{blue}{r_2}$

\vspace{1.5em}
\begin{block}{Corollary 3.1}
    The number of $r$-tuples in each $R^{u, v}$ is $\Theta(\tau)$.
\end{block}

\vspace{1.5em}
\begin{block}{Corollary 3.2}
    The number of all $r$-tuples is $\Theta(n^2\tau)$.
\end{block}
\end{frame}


\section{Algorithms}

\begin{frame}{Timed Transitive Closure Operations}

Directed graph operations:
\begin{itemize}
    \item $\mathcal{N}_{out}(u) = \{ v_1, v_2, \ldots, v_n \}$
    \item $\mathcal{N}_{in}(v) = \{ u_1, u_2, \ldots, u_n \}$
    \item $\mathcal{I}(u, v) = \lambda((u, v))$
\end{itemize}

\vspace{1.5em}
Interval set operations:
\begin{itemize}
    \item Iterating through all intervals in $\mathcal{I}(u, v)$ is $\tau$
    \item Searching in $\mathcal{I}(u, v)$ is $O(\log\tau)$
    \item $\Call{insert}{\mathcal{I}(u, v), l}$
    \item $\Call{insert\mund{}many}{\mathcal{I}(u, v), \{ l_1, l_2, \ldots, l_\kappa \}}$
\end{itemize}

\end{frame}


\subsection{Add Contact}%
\label{subsec:Add Contact}


\begin{frame}{Inserting new contact $(u, v, t)$}
\begin{algorithmic}[1]
    \color{cyan}
    \State $r_{trivial} \gets (u, v, t, t + \delta)$
    \State \Call{insert}{$r_{trivial}$}
    \color{red}
    \State $R_{added} \gets \emptyset$
    \ForAll{$w \in \mathcal{N}_{in}(u)$}
        \State find $r_{tmp} \in R^{w,u}$ with the greater $t^-$ such that $r_{tmp} \prec r_{trivial}$
        \If{such $r$-tuple exists}
            \State $r \gets r_{tmp} \cdot r_{trivial}$
            \State \Call{insert}{$r$}
            \State $R_{added} \gets R_{added} \cup r$
        \EndIf
    \EndFor
    \color{blue}
    \ForAll{$w' \in \mathcal{N}_{out}(v)$}
    \State find $r'_{tmp} \in R^{v,w'}$ with the smallr $t^+$ such that $r_{trivial} \prec r'_{tmp}$
        \If{such $r$-tuple exists}
            \State $r' \gets r_{trivial} \cdot r'_{tmp}$
            \State \Call{insert}{$r'$}
            \color{magenta}
            \ForAll{$r \in R_{added}$}
                \State $r'' \gets r \cdot r'$
                \State \Call{insert}{$r''$}
            \EndFor
        \EndIf
    \EndFor
\end{algorithmic}
\end{frame}

\begin{frame}{Number of $R$-Tuples Touched while Updating Database I}
Given a database containing all $R^{u, v}$ with no unnecessary $r$-tuple and an $add\mund{}contact(u, v, t)$ operation

\vspace{1.5em}
\begin{block}{Hypothesis 4}
    The number of $r$-tuples touched per $R^{u, v}$ is at most $1$

    (not $O(1)$, thank you Marcelo)
\end{block}
\end{frame}

\begin{frame}{Number of $R$-Tuples Touched while Updating Database 2}
\begin{block}{Proof (by cases)}
    \begin{enumerate}
        \item[Case 1] Try adding $\alert{r_{trivial}} = (u, v, t, t + \delta)$ into \alert{$R^{u, v}$}
        \item[Case 2] For $w \in \mathcal{N}_{in}(u)$ such that $w \neq v$
        \begin{itemize}
            \item create $P = \{ r \cdot r_{trivial} \mid r \in R^{w, u} \land r \prec r_{trivial} \}$
            \item order $P$ by departure timestamp
            \item as $p_{i} \subseteq p_{i-1}$, try adding \alert{$\max(P)$} into \alert{$R^{w, v}$}
        \end{itemize}

        \item[Case 3] For $w' \in \mathcal{N}_{out}(v)$ such that $w' \neq u$
        \begin{itemize}
            \item create $P' = \{ r_{trivial} \cdot r' \mid r' \in R^{v, w'} \land r_{trivial} \prec r' \}$
            \item order $P'$ by arrival timestamp
            \item as $p'_i \subseteq p'_{i+1}$, try adding \alert{$\min(P')$} into \alert{$R^{u, w'}$}
        \end{itemize}
        \item[Case 4] For $w \in \mathcal{N}_{in}(u)$, $w' \in \mathcal{N}_{out}(v)$ such that $w \neq v$ and $w' \neq u$
        \begin{itemize}
            \item create $P'' = \{ \max(P) \cdot r' \mid r' \in R^{v, w'} \land \max(P) \prec r' \}$
            \item order $P''$ by arrival timestamp
            \item as $p''_i \subseteq p''_{i+1}$, try adding \alert{$\min(P'')$} into \alert{$R^{w, w'}$}
        \end{itemize}
    \end{enumerate}

    \vspace{.5em}
    \uncover<2>{
        \begin{itemize}
            \item There are at most $1$ tuple being inserted in each case
            \item Each $R^{u,v}$ is touched at most once
        \end{itemize}
    }
\end{block}
\end{frame}


\begin{frame}{Number of $R$-Tuples Touched while Updating Database}
Given an \alert{$add\mund{}contact(u, v, t)$} operation

\vspace{1.5em}
\begin{block}{Corollary 4.1}
    The number of all $r$-tuples touched is at most $n^2$
\end{block}
\end{frame}


\begin{frame}{Inserting new contacts $(u, v, [t_1, t_\kappa])$}
\begin{algorithmic}[1]
    \footnotesize
    \color{cyan}
    \State $R_{trivial} \gets \emptyset$
    \ForAll{$t \in [t_1, t_\kappa]$}
        \State $R_{trivial} \gets R_{trivial} \cup (u, v, t, t + \delta)$
    \EndFor
    \State $\Call{insert\mund{}many}{R_{trivial}}$
    \color{red}
    \State $W_{added} \gets \emptyset$; $\mathcal{R}_{added} \gets \emptyset$
    \ForAll{$w \in \mathcal{N}_{in}(u)$}
    \State $R_{tmp} \gets$ all $r \in R^{w,u}$ such that $r \prec max(R_{trivial})$
        \If{$R_{tmp} \neq \emptyset$}
            \State $R \gets \Call{match\mund{}and\mund{}concat}{R_{tmp}, R_{trivial}}$
            % \State $R \gets \emptyset$
            \State $\Call{insert\mund{}many}{R}$
            \State $W_{added} \gets W_{added} \cup w$
            \State $\mathcal{R}_{added}[w] \gets \mathcal{R}_{added}[w] \cup R$
        \EndIf
    \EndFor
    \color{blue}
    \ForAll{$w' \in \mathcal{N}_{out}(v)$}
        \State $R'_{tmp} \gets$ all $r' \in R^{w,u}$ such that $min(R_{trivial}) \prec r'$
        \If{$R'_{tmp} \neq \emptyset$}
            \State $R' \gets \Call{match\mund{}and\mund{}concat}{R_{trivial}, R'_{tmp}}$
            \State $\Call{insert\mund{}many}{R'}$
            \color{magenta}
            \ForAll{$w \in W_{added}$}
                \ForAll{$R \in \mathcal{R}_{added}[w]$}
                    \State $R'' \gets \Call{match\mund{}and\mund{}concat}{R, R'}$
                    \State $\Call{insert\mund{}many}{R''}$
                \EndFor
            \EndFor
        \EndIf
    \EndFor
\end{algorithmic}
\end{frame}


\begin{frame}{Number of $R$-Tuples Touched while Updating Database}
Given an \alert{$add\mund{}contact(u, v, [t_1, t_\kappa])$} operation
\vspace{1.5em}
\begin{block}{Corollary 4.2}
    The number of all $r$-tuples touched is at most $\kappa n^2$
\end{block}
\end{frame}


\subsection{Can Reach}%
\label{subsec:Can Reach}

\begin{frame}{Checking Whether There are any Journey $u \rightsquigarrow v$ in $[t_1, t_\kappa]$}
\begin{enumerate}
    \item Access $\mathcal{I}(u, v)$
    \item Find any interval $i \in [t_1, t_\kappa]$
    \item If there is, return true, else, return false
\end{enumerate}
\end{frame}


\subsection{Get Journey}%
\label{subsec:Get Journey}

\begin{frame}{Materializing Journey $u \rightsquigarrow v$ in $[t_1, t_\kappa]$ --- Idea I}
    Algorithm idea:
    \vspace{.5em}
    \begin{enumerate}
        \setlength\itemsep{.5em}
        \item Find an initial $r$-tuple depending on some journey characteristic
        \item Perform DFS from $u$ at $t_1$ to $v$ at $t_\kappa$
        \begin{itemize}
            \item filter paths that goes beyond $[t_1, t_\kappa]$
        \end{itemize}
        \item Construct journey by backtracking
    \end{enumerate}
\end{frame}

\begin{frame}{Materializing Journeys $u \rightsquigarrow v$ in $[t_1, t_\kappa]$ --- Idea II}
    Augment $r$-tuples with the next first vertex in $\mathcal{J}$

    \vspace{1.5em}
    Example:
    \vspace{.5em}
    \begin{itemize}
        \setlength\itemsep{.5em}
        \item $\mathcal{J} = \{ c_1, c_2, \ldots, c_k \}$, where $c_i = ((u_i, v_i), t_i)$
        \item $r = (u, v, t^-, t^+, w)$
        \begin{itemize}
            \item $u = u_1$, ~~$v = v_k$
            \item $t^- = t_1$, ~$t^+ = t_k + \delta$
            \alert{\item $w = v_1$}
        \end{itemize}
    \end{itemize}


    \vspace{1.5em}
    Algorithm idea:
    \vspace{.5em}
    \begin{enumerate}
        \setlength\itemsep{.5em}
        \item Find an initial $r$-tuple depending on some journey characteristic
        \item Find next valid $r$-tuple in $R^{w,v}$ and cocatenate it until $w = v$
    \end{enumerate}
\end{frame}

\begin{frame}{Materializing Journeys $u \rightsquigarrow v$ in $[t_1, t_\kappa]$ --- Idea III}
    Augment $r$-tuples with a pointer to the next $r$-tuple in $R^{w, v}$

    \vspace{1.5em}
    Example:
    \vspace{.5em}
    \begin{itemize}
        \setlength\itemsep{.5em}
        \item $\mathcal{J} = \{ c_1, c_2, \ldots, c_k \}$, where $c_i = ((u_i, v_i), t_i)$
        \item $r = (u, v, t^-, t^+, w)$
        \begin{itemize}
            \item $u = u_1$, ~~$v = v_k$
            \item $t^- = t_1$, ~$t^+ = t_k + \delta$
            \alert{\item $p = next(R^{v_1, v_k})$}
        \end{itemize}
    \end{itemize}


    \vspace{1.5em}
    Algorithm idea:
    \vspace{.5em}
    \begin{enumerate}
        \setlength\itemsep{.5em}
        \item Find an initial $r$-tuple depending on some journey characteristic
        \item Use $p$ to go to $r$-tuple in $R^{w,v}$ and cocatenate it until $w = v$
    \end{enumerate}
\end{frame}


% \section{Data Structure}

\section{Conclusion}

\begin{frame}{Conclusion}

There is no conclusion yet

\end{frame}

\begin{frame}{What next?}

\begin{itemize}
    \setlength\itemsep{1.5em}
    \item Does $\Call{insert\mund{}many}{\mathcal{I}(u, v), \{ l_1, l_2, \ldots, l_\kappa \}}$ really helps?
    \begin{itemize}
        \item maybe the second algorithm doesn't improve the first one at all
        \item maybe the second algorithm improves sometimes
        \item maybe the second algorithm improves everytime
    \end{itemize}
    \item Is there some magic data structure that can help us
    \item Can we augment $r$-tuples to speed-up journey materialization
    \item Which types of journeys can we really materialize
    \item Improve/simplify manuscript
    \item Publish
    \item Start article for decremental case
\end{itemize}
\end{frame}


\begin{frame}
    \centering\Huge Merci beaucoup!
\end{frame}

\begin{frame}
\printbibliography
\end{frame}


\end{document}
