\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{xcolor}
\usepackage{algpseudocode}

\newcommand{\mybreak}{\vspace{1cm}\noindent\rule{\textwidth}{1pt}\vspace{1cm}}
\newcommand{\mycomment}[1]{\vspace{0.5cm}{\color{blue} #1}\vspace{0.5cm}}

\setlength{\parindent}{0em}


\begin{document}

\mycomment{
Dear Editor, \\[0.3cm]
We thank you for considering our paper for publication and all reviewers for insightful comments and helpful suggestions.
We have considered all comments and suggestions carefully in the revised version of the manuscript.
Both our changes in the manuscript and responses to the reviewers (in the present document) are highlighted in blue.
We have also improved an operation (and its implementation) thanks to the suggestions of reviewer 1,%
and performed a few experiments to provide some empirical observations related to the scaling of our data structure, based on the suggestion of reviewer 2.
% We hope we have addressed all comments satisfactorily and we remain available for further clarifications, if necessary.  \\[0.3cm]
\smallskip

Best regards,\smallskip\\
The authors
}


\mybreak


\textbf{Reviewer \#1:} The paper presents a data structure for answering reachability queries in temporal graphs.
The data structure supports insertion of directed time edges (\textit{i.e.} a node u has a link to node $v$ at time $t$) in arbitrary order (\textit{i.e.} non-chronologically).
Such a data structure is useful for the analysis of temporal graphs for which additional data becomes available over time.
This is a timely and relevant topic.
For a temporal graph with n nodes and lifetime $T$, the data structure uses space $O(n^2 T)$, answers reachability queries (is $u$ reachable from $v$ during time interval $[a,b]$?) in $O(\log T)$ time, constructs the corresponding journey in $O(k \log T)$ time if the journey requires $k$ links, and supports updates (insertion of a time edge) in amortized $O(n^2 \log T)$ time.
The data structure is based on storing non-dominated reachability tuples in a balanced search tree for each pair of nodes.
The techniques used in the paper are fairly standard, but applied in a non-obvious way.
Therefore, I think the paper makes a valuable contribution that merits publication.
The paper is also well written, apart from minor English language issues.
There is only one technical point that, in my opinion, is not sufficiently clear in the current version and requires a more convincing argumentation.
I recommend acceptance subject to a minor revision addressing the points below.


\mycomment{
We thank the reviewer for summarizing our key contributions and for recognizing their
relevance. We have considered all comments and suggestions carefully. In particular, the reviewer was right about the invariants of self-balancing trees,%
which led us to improve the removal operation in a new, and actually simpler version (details below).
}

Main technical issue:
(line numbers refer to those printed in the margin)

page 12, line 8: You use self-balancing search trees (e.g. red-black trees) and have shown that the removal operation for d consecutive elements can be done in $O(d + \log T)$ time.
Then you claim that future operations on the balanced search tree can still be done in $O(\log T)$ time because its depth is still $O(\log T)$.
I don't think this is argument is sufficient.
For example, assume that you use a red-black tree.
The invariant for red-black trees is that all paths from a node to a leaf descendant of the node have the same number of black nodes.
This invariant could clearly be violated after the removal of elements between $I_1$ and $I_2$.
How can you be sure that you can continue to use the standard operations for red-black trees in future insertions to keep the tree balanced if the current tree no longer satisfies the invariants for red-black trees?
I think you need to justify more carefully how the balanced search tree whose invariants have just been destroyed by deleting a number of consecutive elements (and some of their ancestors) can continue to be used as a balanced search tree for future operations.

\mycomment{
  This observation is correct, thank you. Interestingly, considering this issue led us to find an even simpler solution to the removal of intervals.
  Namely, we now rely on SPLIT and JOIN operations on the tree, which are known to be feasible on red-black trees in time $O(\log \tau)$. As a result, the proof is simpler now than before.
  Please see proof of Lemma 3.
}

Two further remarks about this:

\begin{enumerate}
  \item
    You delete the range from $I_1$ to $I_2$ by walking from $I_1$ and $I_2$ to the common ancestor $I_A$.
    Both of these walks are only $O(\log T)$ steps long because the tree is balanced.
    Doesn't that mean that you can do the deletion in $O(\log T)$ steps instead of $O(d + \log T)$ steps?
    Do you assume that all the deleted nodes need to be visited in order to de-allocate their memory and this is the reason why you require $O(d)$ time to delete d nodes?
    If one assumes that unused memory is freed by a garbage collector, $O(\log T)$ time might be enough for the deletion.

  \mycomment{
    Indeed, we realized this by reading the above comment.
    The fact that the cost does not depend on $d$, but only on $\log \tau$ is even clearer in the new version based on SPLIT and JOIN operations (see above).
  }

  \item
    A simple alternative to self-balancing search trees may be to use a complete binary tree with $T$ leaves (one leaf for each time step in the lifetime of the graph).
    An interval $(t1,t2)$ is stored at the leaf for $t1$.
    All paths from the root to a leaf have length $O(\log T)$.
    The nodes that correspond to leaves that currently store an interval, and all their ancestors, are active, and all other nodes are present but inactive.
    This should allow you to implement all the operations you need in the required time bounds, but without having to worry about maintaining invariants of self-balancing trees after deletions.
    The disadvantage is that each binary tree will use $O(T)$ memory no matter how many intervals it stores, but this is still within your desired overall space bound of $O(n^2 T)$.

    \mycomment{
      We thank the reviewer for the suggestion. At first, we considered this strategy, which indeed preserves worst-case space complexities, however at the cost of worse average-case complexity.
      Eventually, we found out the SPLIT and JOIN operations, which we used to solve the problem without this disadvantage.
  }

\end{enumerate}

Other suggested corrections (mostly very minor language issues):
(line numbers refer to those printed in the margin)

page 2, line 6: "with each others" -> "with each other"

page 3, line 10: "a set of contact" -> "a set of contacts"

page 3, line 21: "if and only if and vertex" -> "if and only if vertex"

page 7, line 37: The QED box at the end of the proof of Lemma 1 is missing, and the same comment applies to all proofs in the rest of the paper as well.

page 10, line 33: "returns the containing the earliest interval" -> "returns the earliest interval"

page 10, line 44: "is equals to" -> "is equal to"

page 11, line 7-13: The caption of Fig. 3 uses $t_1$ and $t_2$ as endpoints of $I$ in line 7 but then uses $t^+$ and $t^-$ in line 10.
Please check and make it consistent.
Also, since elsewhere \textproc{insert} takes four arguments, you may wish to write $\textproc{insert}(u,v,t^-,t^+)$ instead of $\textproc{insert}(u,v,I)$ in line 7.

page 11, line 9: "after the inserting" -> "after inserting"

page 11, line 29: You write: "which correspond to the first and last nodes to be removed": I think $I_1$ only needs to be removed if it ends at $t^+$, but not if it ends before $t^+$ (and starts before $t^-$).
Similarly, $I_2$ only needs to be removed if it starts at $t^-$, but not if it starts after $t^-$ (and ends after $t^+$).
Only the intervals strictly between $I_1$ and $I_2$ always need to be removed.
Please check and clarify.

page 12, line 3: "one of its child" -> "one of its children"

page 13, line 7: "encapsulate" -> "encapsulates"

page 13, line 28: "needs considering" -> "needs to consider"

page 13, line 32: "$T(u,w)$" -> "$T(u,w^+)$"

page 14, line 42: "or $t^+ <= t_2$" -> "or $t^+ > t_2$"

page 17: Please check the references carefully for complete bibliographic information. Some issues I noticed are:

[5]: No information about the title of the book or proceedings in which the article has been published. Do you mean the conference paper published at PSTV 1995?

[8]: No information about where the article has been published. (I think the paper appeared at MFCS 2019.)

[13]: "$o(s+h)$" should be "$O(s+h)$"

[15]: No information about where the article has been published. (You can cite the arXiv version if no other version has been published.)

\mycomment{
  All done, thank you.
}

\mybreak

\textbf{Reviewer \#2:}
1. The main component of introduced data structure is a generalization of transitive closure called Timed Transitive Closure (TTC) where The main components of TTCs are called reachability tuples (R-tuple).
In this research, the authors introduce a data structure to solve the dynamic connectivity problem in temporal graph.
Which can be very important in storing data related to dynamic network components and applications of high importance in various fields of data mining and recommender systems, so the issue is important and up-to-date.

\mycomment{
  We thank the reviewer for recognizing the relevance of our contributions. We have considered all the comments and revised the manuscript accordingly.
}

2. The innovation used in this research is to introduce a data structure to maintain accessibility information for all nested time periods.
The author has been able to study the relevant studies well and the structure of the article has the necessary coherence and relevance.
To improve the quality of the article, it is suggested:

\begin{enumerate}
\item[i)] The importance of each operator introduced in this data structure and its application example that shows its importance in the real world is not explicitly mentioned.

  \mycomment{The motivation for such a data structure is discussed in the first half of the introduction. In addition, we have mentioned in the revised version some specific examples of use in the same paragraph that the operations are defined.}

  \item[ii)] Also there is no implementation of dynamicity parameter such as $\delta$ (latency) on operators such as inclusion and concatenation where defined in this research (page 6 line 26).

    \mycomment{
      We see the point of the reviewer. In fact, $\delta$ is taken into account in these operations, but implicitly so. Indeed, it is encoded within the arrival date of an $R$-$tuple$, starting with the first base case of trivial contacts, whose arrival time correspond to the departure time plus $\delta$. Then, these latencies propagate transparently upon composition.

      As of the dynamicity of the parameter itself, a future generalization of this work might consider dynamic versions of it. In the present work, we have considered any possible values of $\delta$, but assumed to remain fixed. In order to reflect the impact of several values of $\delta$, we have performed some experiments (see below) that consider either $0$ or $1$, which reflects the essential distinction between strict and non-strict journeys.
  }

\item[iii)] On page 6 of line 18, only an unspecified number of time intervals are mentioned ($T = [1; \tau]$) and the effect of length and number of time intervals on the mentioned parameters is not investigated.

  \mycomment{
    It seems that page 6 of the submitted version does not have any text on line 18. We think that the reviewer might actually refer to Page $5$ of the paper (Page $7$ in the submitted PDF), whose line 18 indeed contains the definition of the lifetime ``$\mathcal{T}=[1,\tau]$''. The length of the lifetime thus have duration $\tau$ and may contain essentially $\Theta(\tau^2)$ different interval, but focusing only on irredundant ones (like in our data structure) reduces this amount to $O(\tau)$. The arguments are present in the proof of Lemma~2. A sentence highlighting this can be found in the revised version just before the lemma (which hasn't changed).

  }


\item[iv)] Since the structure introduced in this study is for dynamic communication, so the study of dynamic parameters on the proposed structure is of great importance and it is suggested that the structure be implemented on the standard data set and the results be shown on the mentioned parameters.

  \mycomment{
We agree with the reviewer that experiments help to understand the behavior of our data structure, even though in this paper,%
in the spirit of typical work in data structures, we were mostly concerned with worst-case asymptotics.%
So we have performed some experiments for studying how the number of $R$-$tuples$ scales with the number of contacts,%
using a simple synthetic model where contacts are added uniformly at random among the set of possible pairs of nodes and possible times.%
It turns out that the experiments were very instructive for us and for the paper.%
The new content is about two pages long, summarized as follows.
The number of $R$-$tuples$ exhibits different regimes over the insertions of contacts.
It is first linear, due to the independence among the first few contacts, then it starts increasing superlinearly, due to the composition of different journeys,%
and it converges eventually to the number of contacts itself, as anticipated by the content of our theoretical section (see Lemma~2, for example).%
The experiments helped noticing surprising phenomena, namely, that the number of R-tuples may actually decrease when the graph becomes very dense, but not yet saturated,%
before increasing again when it becomes saturated.%%%% TODO faltou indicar na rebutal letter o porque que "number of R-tuples may actually decrease when the graph becomes very dense"
}
\end{enumerate}

3. In some sentences, there is not enough accuracy to express the content and there is a need to reconsider.
For example, on page 3 lines 20-21 "if and only if and vertex".
Also, here are some grammatical and punctuation mistakes.

On page 1 line 25 "an arbitrary" and line 35 "a given time".

On page 2 line 5 "each other", line 30 "the network (period or semicolon) it".

On page 4 line 29 "; however" and line 30 ", and.

On page 5 line 14 "is presented".

On page 8 line 31 "downside" and line 34 ", and".

On page 10 line 44 "is equal" instead of "is equals to".

On page 12 line 2 "children" and line 4 "downside".

\mycomment{
  All done, thank you.
}
\end{document}
