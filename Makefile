all: arxiv.pdf snam.pdf rebutal-letter.pdf

arxiv.pdf:
	$(MAKE) -C arxiv manuscript
	cp arxiv/manuscript.pdf arxiv.pdf
	$(MAKE) -C arxiv clean

snam.pdf:
	$(MAKE) -C snam manuscript
	cp snam/template.pdf snam.pdf
	$(MAKE) -C snam clean

rebutal-letter.pdf:
	$(MAKE) -C rebutal-letter letter
	cp rebutal-letter/letter.pdf rebutal-letter.pdf
	$(MAKE) -C rebutal-letter clean


clean:
	$(MAKE) -C arxiv clean
	$(MAKE) -C snam clean
	$(MAKE) -C rebutal-letter clean
	rm arxiv.pdf snam.pdf rebutal-letter.pdf
